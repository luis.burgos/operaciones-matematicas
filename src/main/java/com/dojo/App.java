package com.dojo;

public class App {
    public String suma() {
        System.out.println("*** Suma en duro ***");
        return "Suma: 5 + 5 = " + (5+5);
    }

    public String suma(int a, int b) {
        System.out.println("*** Suma con numero enviados ***");
        return "Suma: " + a + " + " + b + " = " + (a+b);
    }

    public static void main(String[] args) {  
        System.out.println(new App().suma());
        System.out.println(new App().suma(5, 5));
    }
}