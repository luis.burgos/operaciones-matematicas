# Operaciones Matemáticas

**Ejercicio 1: Operaciones matemáticas**

Operación: *Suma*

Objetivos:
* Crear un test que falle en sumar dos números 
* Crear la clase que devuelva la suma en duro para pasar la prueba
* Crear el cuerpo de la clase para que haga operaciones 
